﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using GuiNote.Models;
using GuiNote.Views;
using ReactiveUI;
using SymbLab;
using Tree;

namespace GuiNote.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private string _code="/*default example\nstart here*/var a;\nbegin\n  a:=1+2;\nend";
        private string _error;
        private string _dictionary;
        private string _result;
        private bool _isClickable;
        private string _dataFile = "code.tmp";
        private int sepI = 0;
        private ObservableCollection<ObsCollectionTree> _collection;
        private int SepI => sepI++;

        public bool IsClickable
        {
            get => _isClickable;
            set =>  this.RaiseAndSetIfChanged(ref _isClickable, value);
        }

        public string Dictionary
        {
            get => _dictionary;
            private set => this.RaiseAndSetIfChanged(ref _dictionary, value);
        }

        public string Result
        {
            get => _result;
            private set => this.RaiseAndSetIfChanged(ref _result, value);
        }

        public string Error
        {
            get => _error;
            private set => this.RaiseAndSetIfChanged(ref _error, value);
        }

        public string Code
        {
            get => _code;
            set => this.RaiseAndSetIfChanged(ref _code, value);
        }

        public ObservableCollection<ObsCollectionTree> Collection
        {
            get => _collection;
            set => this.RaiseAndSetIfChanged(ref _collection, value);
        }

        public async void Translate()
        {
            IsClickable = false;
            _dictionary = _error = _result = String.Empty;
            File.WriteAllText(_dataFile,Code);
            File.WriteAllText("res.tmp",String.Empty);
            await  Task.Run(() => { PrepareLib.Prepare.StartPrepare("res.tmp", "code.tmp"); });
            SymbAutomat automat = new SymbAutomat(new ContextData(), new SymbLib());
            await Task.Run(()=>automat.StartConvert("res.tmp"));
            Error = automat.Errors;
            
            Result = String.Join("\n", automat.Data);
            Dictionary = automat.SymbLib.ToString();
            try
            {
                var t = new TreeBilder(automat.Data, automat.SymbLib);
                Collection = await ObsCollectionTree.GetObsCollection(t.Tree);
            }
            catch (Exception e)
            {
                Error += e.Message;
            }

           
            IsClickable = true;
        }
        public async Task Open()
        {
            var dialog = new OpenFileDialog();
            string[] result = null;
            dialog.Filters.Add(new FileDialogFilter() {Name = "Text", Extensions = {"txt"}});
            dialog.Filters.Add(new FileDialogFilter() {Name = "Code", Extensions = {"code","cd","tmp"}});
            dialog.Filters.Add(new FileDialogFilter {Name = "All", Extensions = {"*"}});
            result = await dialog.ShowAsync(new Window());
            if (result != null)
            {
                _dataFile = result.First();
                Code = File.ReadAllText(result.First());
            }
        }
        public async Task Save()
        {
            var dialog = new SaveFileDialog();
            dialog.Filters.Add(new FileDialogFilter {Name = "Text", Extensions = {"txt"}});
            dialog.Filters.Add(new FileDialogFilter {Name = "Code", Extensions = {"code","cd","tmp"}});
            dialog.Filters.Add(new FileDialogFilter {Name = "All", Extensions = {"*"}});
            
            var result = await dialog.ShowAsync(new Window());
            if (result != null)
            {
                _dataFile = result;
                File.WriteAllText(result, Code);
            }
        }
        public void Exit()

        {
           
            //Environment.Exit(1);
        }
    }
}