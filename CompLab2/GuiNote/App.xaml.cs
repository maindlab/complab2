using Avalonia;
using Avalonia.Markup.Xaml;

namespace GuiNote
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}