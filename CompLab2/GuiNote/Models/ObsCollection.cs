using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Tree;

namespace GuiNote.Models
{
    public class ObsCollectionTree
    {
        public string Name { get; set; }
        public int? Ident { get; set; }
        public ObservableCollection<ObsCollectionTree> Trees { get; set; }
        private  ObsCollectionTree(){}

        private static async  Task<ObsCollectionTree> GetObsTree(SymbTree tree)
        {
            var obs = new ObsCollectionTree();
            await Task.Run(async () =>
            {
               
                obs.Name = tree.Comment;
                if (tree.Data != null) obs.Ident = tree.Data;
                if (tree.Trees != null)
                {
                    obs.Trees = new ObservableCollection<ObsCollectionTree>();
                    foreach (var elem in tree.Trees)
                    {
                        obs.Trees.Add(await GetObsTree(elem));
                    }
                }
            });
            return obs;
        }

        public static async Task<ObservableCollection<ObsCollectionTree>> GetObsCollection(SymbTree tree)
        {
            var c = new ObservableCollection<ObsCollectionTree>();
            c.Add(await GetObsTree(tree));
            return c;
        }
    }
}