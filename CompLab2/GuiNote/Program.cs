﻿using System;
using System.ComponentModel;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Remote;
using Avalonia.Logging.Serilog;
using GuiNote.ViewModels;
using GuiNote.Views;

namespace GuiNote
{
    class Program
    {
        // Initialization code. Don't use any Avalonia, third-party APIs or any
        // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
        // yet and stuff might break.
        public static void Main(string[] args) => BuildAvaloniaApp().Start(AppMain, args);

        // Avalonia configuration, don't remove; also used by visual designer.
        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToDebug()
                .UseReactiveUI();

        // Your application's entry point. Here you can initialize your MVVM framework, DI
        // container, etc.
        private static void AppMain(Application app, string[] args)
        {
            var window = new MainWindow
            {
                DataContext = new MainWindowViewModel(),
                
            };
            
            window.Closing +=Exit;
            try
            {
                window.Icon = new WindowIcon("./Assets/avalonia-logo.ico");
            }
            catch
            {
                
            }

          
            window.WindowState = WindowState.Maximized;
            app.Run(window);
           

        }

        private static void Exit(object o, CancelEventArgs args)
        {
            Environment.Exit(1);
        }
    }
}
