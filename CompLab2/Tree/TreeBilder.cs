using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using SymbLab;

namespace Tree
{
    public class TreeBilder
    {
        private IList<int> _list;
        private SymbLib _lib;
        private int counter = 0;
        public SymbTree Tree { get; private set; }

        public TreeBilder(IList<int> list, SymbLib lib)
        {
            _lib = lib;
            _list = list;
            Tree = GetProgram();
        }

        private SymbTree GetProgram()
        {
            var t = new SymbTree();
            t.Data = null;
            t.Comment = "Program";
            t.Trees = new List<SymbTree>();
            t.Trees.Add(GetVarsInit());
            t.Trees.Add(GetCountDoing());
            return t;
        }

        private SymbTree GetVarsInit()
        {
            var t = new SymbTree();
            t.Data = null;
            t.Comment = "Var list";
            t.Trees = new List<SymbTree>();
            t.Trees.Add(GetVar());
            t.Trees.Add(GetListIdent());
            return t;
        }

        private SymbTree GetVar()
        {
            var t = new SymbTree();
            if (_list[counter] == 1000001)
            {
                t.Data = 1000001;
                t.Comment = "Var";
                counter++;
                return t;
            }
          
                throw new Exception("no var position" + counter);
            
        }

        private SymbTree GetListIdent()
        {
            var t = new SymbTree();
            var localcounter = counter;
            t.Comment = "list of ident";
            t.Data = null;
            t.Trees = new List<SymbTree>();
            try
            {
                t.Trees.Add(GetIdent());
                t.Trees.Add(GetSemicolon());
            }
            catch
            {
                counter = localcounter;
                t.Trees = new List<SymbTree>();
                t.Trees.Add(GetIdent());
                t.Trees.Add(GetComma());
                t.Trees.Add(GetListIdent());
            }

            return t;
        }

        private SymbTree GetIdent()
        {
            var t = new SymbTree();
            if (_list[counter] / 10000 == 104)
            {
                t.Data = _list[counter];
                t.Comment = "identificator is " + _lib.GetValue(_list[counter]);
                counter++;
                return t;
            }

            throw new Exception("no ident position" + counter);
        }

        private SymbTree GetComma()
        {
            var t = new SymbTree();
            if (_list[counter] == 1030004)
            {
                t.Data = _list[counter];
                t.Comment = "comma is " + _lib.GetValue(_list[counter]);
                counter++;
                return t;
            }

            throw new Exception("no comma position" + counter);
        }

        private SymbTree GetSemicolon()
        {
            var t = new SymbTree();
            if (_list[counter] == 1030003)
            {
                t.Data = _list[counter];
                t.Comment = "comma is " + _lib.GetValue(_list[counter]);
                counter++;
                return t;
            }

            throw new Exception("no semicolon position" + counter);
        }

        private SymbTree GetCountDoing()
        {
            var t = new SymbTree();
            t.Comment = "count doing ";
            t.Trees = new List<SymbTree>();
            t.Trees.Add(GetBegin());
            t.Trees.Add(GetListOperations());
            t.Trees.Add(GetEnd());
            return t;
        }

        private SymbTree GetBegin()
        {
            var t = new SymbTree();
            if (_list[counter] == 1000002)
            {
                t.Data = _list[counter];
                t.Comment = "begin";
                counter++;
                return t;
            }

            throw new Exception("no begin position" + counter);
        }

        private SymbTree GetListOperations()
        {
            var t = new SymbTree();
            var localcounter = counter;
            t.Comment = "list of operations";
            t.Data = null;
            t.Trees = new List<SymbTree>();
            try
            {
                t.Trees.Add(GetOperator());
                t.Trees.Add(GetListOperations());
            }
            catch
            {
                counter = localcounter;
                t.Trees = new List<SymbTree>();
                t.Trees.Add(GetOperator());
            }

            return t;
        }

        private SymbTree GetOperator()
        {
            var t = new SymbTree();
            var localcounter = counter;
            t.Comment = "operation";
            t.Data = null;
            t.Trees = new List<SymbTree>();
            try
            {
                t.Trees.Add(GetOperatorSet());
            }
            catch
            {
                counter = localcounter;
                t.Trees.Add(GetCountDoing());
            }

            return t;
        }

        private SymbTree GetOperatorSet()
        {
            var t = new SymbTree();
            t.Comment = "operation set";
            t.Data = null;
            t.Trees = new List<SymbTree>();
            t.Trees.Add(GetIdent());
            t.Trees.Add(GetSetOperation());
            t.Trees.Add(GetExpression());
            t.Trees.Add(GetSemicolon());

            return t;
        }

        private SymbTree GetExpression()
        {
            var t = new SymbTree();
            var localcounter = counter;
            t.Comment = "expression";
            t.Data = null;
            t.Trees = new List<SymbTree>();
            try
            {
                t.Trees.Add(GetUnaryOperation());
                t.Trees.Add(GetUnderExpression());
            }
            catch
            {
                counter = localcounter;
                t.Trees = new List<SymbTree>();
                t.Trees.Add(GetUnderExpression());
            }

            return t;
        }

        private SymbTree GetUnderExpression()
        {
            var t = new SymbTree();
            var localcounter = counter;
            t.Comment = "list of operations";
            t.Data = null;
            t.Trees = new List<SymbTree>();
            try
            {
                t.Trees.Add(GetLeftBraket());
                t.Trees.Add(GetExpression());
                t.Trees.Add(GetRightBraket());
            }
            catch
            {
                try
                {
                    counter = localcounter;
                    t.Trees = new List<SymbTree>();
                    t.Trees.Add(GetOperand());
                    t.Trees.Add(GetBinaryOperation());
                    t.Trees.Add(GetUnderExpression());
                   
                }
                catch
                {
                    counter = localcounter;
                    t.Trees = new List<SymbTree>();
                    t.Trees.Add(GetOperand());
                }
            }

            return t;
        }

        private SymbTree GetBinaryOperation()
        {
            var t = new SymbTree();
            if (_list[counter] / 10000 == 101)
            {
                t.Data = _list[counter];
                t.Comment = "binary operator is " + _lib.GetValue(_list[counter]);
                counter++;
                return t;
            }

            throw new Exception("no binary position" + counter);
        }

        private SymbTree GetOperand()
        {
            var t = new SymbTree();
            var localcounter = counter;
            t.Comment = "operand";
            t.Data = null;
            t.Trees = new List<SymbTree>();
            try
            {
                t.Trees.Add(GetIdent());
            }
            catch
            {
                counter = localcounter;
                t.Trees = new List<SymbTree>();
                t.Trees.Add(GetConst());
            }

            return t;
        }

        private SymbTree GetConst()
        {
            var t = new SymbTree();
            if (_list[counter] / 10000 == 105)
            {
                t.Data = _list[counter];
                t.Comment = "const is " + _lib.GetValue(_list[counter]);
                counter++;
                return t;
            }

            throw new Exception("no const position" + counter);
        }

        private SymbTree GetRightBraket()
        {
            var t = new SymbTree();
            if (_list[counter] == 1030001)
            {
                t.Data = _list[counter];
                t.Comment = ")";
                counter++;
                return t;
            }

            throw new Exception("no ) position" + counter);
        }

        private SymbTree GetLeftBraket()
        {
            var t = new SymbTree();
            if (_list[counter] == 1030001)
            {
                t.Data = _list[counter];
                t.Comment = "(";
                counter++;
                return t;
            }

            throw new Exception("no ( position" + counter);
        }

        private SymbTree GetUnaryOperation()
        {
            var t = new SymbTree();
            if (_list[counter] == 1010001)
            {
                t.Data = _list[counter];
                t.Comment = "unary -";
                counter++;
                return t;
            }

            throw new Exception("no unary minus position" + counter);
        }

        private SymbTree GetSetOperation()
        {
            var t = new SymbTree();
            if (_list[counter] == 1020001)
            {
                t.Data = _list[counter];
                t.Comment = "set :=";
                counter++;
                return t;
            }

            throw new Exception("no set position" + counter);
        }

        private SymbTree GetEnd()
        {
            var t = new SymbTree();
            if (_list[counter] == 1000003)
            {
                t.Data = _list[counter];
                t.Comment = "end";
                counter++;
                return t;
            }

            throw new Exception("no end position" + counter);
        }
    }
}