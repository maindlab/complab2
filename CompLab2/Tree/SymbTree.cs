using System.Collections.Generic;

namespace Tree
{
    public class SymbTree
    {
        public string Comment { get; set; }
        public int? Data { get; set; } = null;
        public List<SymbTree> Trees { get; set; }
    }
}