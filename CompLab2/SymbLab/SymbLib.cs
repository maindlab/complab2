using System.Collections.Generic;
using System.Linq;

namespace SymbLab
{
    public class SymbLib
    {
        private readonly Dictionary<string,int> _keyWords;
        private readonly Dictionary<string,int> _operationsWords;
        private readonly Dictionary<string,int> _doubleOperationsWords;
        private readonly Dictionary<string,int> _specialWords;
        private readonly Dictionary<string,int> _identWords;
        private readonly Dictionary<string,int> _constWords;

        public string GetValue(int i)
        {
            return _keyWords.ContainsValue(i) ? _keyWords.First(x => x.Value == i).Key :
                _operationsWords.ContainsValue(i) ? _operationsWords.First(x => x.Value == i).Key :
                _doubleOperationsWords.ContainsValue(i) ? _doubleOperationsWords.First(x => x.Value == i).Key :
                _specialWords.ContainsValue(i) ? _specialWords.First(x => x.Value == i).Key :
                _identWords.ContainsValue(i) ? _identWords.First(x => x.Value == i).Key :
                _constWords.ContainsValue(i) ? _constWords.First(x => x.Value == i).Key : null;
        }
        public int GetKeyNumb(string s)
        {
            if (!_keyWords.ContainsKey(s)) return -1;
            return _keyWords[s];
        }

        public int GetWordNumb(string s)
        {
            var i = GetKeyNumb(s);
            if (i == -1) i = GetIdentNumb(s);
            return i;
        }

        public int GetOperationNumb(string s)
        {
            if (!_operationsWords.ContainsKey(s)) return -1;
            return _operationsWords[s];
        }
        public int GetDoubleNumb(string s)
        {
            if (!_doubleOperationsWords.ContainsKey(s)) return -1;
            return _doubleOperationsWords[s];
        }
        public int GetSpecialNumb(string s)
        {
            if (!_specialWords.ContainsKey(s)) return -1;
            return _specialWords[s];
        }
        public int GetIdentNumb(string s)
        {
            if (!_identWords.ContainsKey(s)) _identWords.Add(s,1040001+_identWords.Count);
            return _identWords[s];
        }
        public int GetConstNumb(string s)
        {
            if (!_constWords.ContainsKey(s)) _constWords.Add(s,1050001+_constWords.Count);
            return _constWords[s];
        }
        public SymbLib()
        {
            _keyWords = new Dictionary<string,int>
            {           
                { "var",1000001},
                { "end",1000003},
                { "begin",1000002},
                { "while",1000004},
                { "do",1000005}
            };
            _operationsWords = new Dictionary<string,int>
            {
                {"-",1010001},
                {"+",1010002},                
                {"*",1010003},
                {"/",1010004},
                {">",1010005},
                {"<",1010006},
                {"=",1010007},            
            };
            _doubleOperationsWords = new Dictionary<string,int>
            {
                {":=",1020001},
            };
            _specialWords = new Dictionary<string,int>
            {
                {"(",1030001},
                {")",1030002},
                {";",1030003},
                {",",1030004},
            };
            _identWords = new Dictionary<string,int>();
            _constWords = new Dictionary<string,int>();

        }
        
        public override string ToString()
        {
            var result = string.Empty;
            foreach (var (key, value) in _keyWords)
            {
                result += $"{key} {value}\n";
            }

            foreach (var (key, value) in _identWords)
            {
                result += $"{key} {value}\n";
            }

            foreach (var (key, value) in _operationsWords)
            {
                result += $"{key} {value}\n";
            }
            foreach (var (key, value) in _doubleOperationsWords)
            {
                result += $"{key} {value}\n";
            }
            foreach (var (key, value) in _specialWords)
            {
                result += $"{key} {value}\n";
            }
            foreach (var (key, value) in _constWords)
            {
                result += $"{key} {value}\n";
            }

            return result;
        }
    }
}