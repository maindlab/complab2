namespace SymbLab
{
    public enum Context
    {
        None,
        Space,
        Line,
        Word,
        Const,
        Operation,
        DoubleOperation,
        Special
    }
}