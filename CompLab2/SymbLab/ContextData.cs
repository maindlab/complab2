using System;
using System.Collections.Generic;

namespace SymbLab
{
    public class ContextData
    {
        public ICollection<IContextSimb> Collection { get;}

        public ContextData()
        {
            Collection = new List<IContextSimb>{new SpaceContext(),new ConstContext(),new WordContext(),new LineContext(),new OperationContext(),new SpecialContext(),new DoubleOperationContext()};
        }
        class SpaceContext:IContextSimb
        {
            public Context Context => Context.Space;
            public bool IsForThisContext(char c)
            {
                return c == ' ';
            }
        }

        class LineContext:IContextSimb
        {
            public Context Context => Context.Line;
            public bool IsForThisContext(char c)
            {
                return c == '\n';
            }
        }

        class WordContext:IContextSimb
        {
            public Context Context => Context.Word;
            public bool IsForThisContext(char c)
            {
                return char.IsLetter(c);
            }
        }

        class ConstContext:IContextSimb
        {
            public Context Context => Context.Const;
            public bool IsForThisContext(char c)
            {
                return char.IsDigit(c);
            }
        }

        class OperationContext:IContextSimb
        {
            public Context Context => Context.Operation;
            public bool IsForThisContext(char c)
            {
                return c == '+' || c == '-' || c == '*' ||c == '/' || c == '=';
            }
        }
        class DoubleOperationContext:IContextSimb
        {
            public Context Context => Context.DoubleOperation;
            public bool IsForThisContext(char c)
            {
                return c == ':';
            }
        }
        class SpecialContext:IContextSimb
        {
            public Context Context => Context.Special;
            public bool IsForThisContext(char c)
            {
                return c == '(' || c == ')' || c == ';' ||c == ',';
            }
        }
    }
}
