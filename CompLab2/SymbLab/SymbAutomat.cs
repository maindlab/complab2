using System;
using System.Collections.Generic;
using System.IO;

namespace SymbLab
{
    public class SymbAutomat
    {
        private ContextData _contextData;
        private SymbLib _lib;
        private int _symbnumb ;
        private int _symbline;
        private string _errors;
        private Context _localContext;
        private string _memory;
        private List<int> _data;
        public SymbLib SymbLib => _lib;
        public string Errors => _errors;
        public IList<int> Data => _data;
        public SymbAutomat(ContextData contextData, SymbLib lib)
        {
            _contextData = contextData;
            _lib = lib;
            _symbnumb = 0;
            _symbline = 0;
            _errors=String.Empty;
            _localContext = Context.None;
            _memory=String.Empty;
            _data = new List<int>();
        }

        public void StartConvert(string fileName)
        {
            using (var stream =new StreamReader(fileName))
            {
                StartConvert(stream);
            }
        }
        public void StartConvert(StreamReader stream)
        {
            int i;
            while ((i = stream.Read())!=-1)
            {
                var c = (char) i;
                Context context=Context.None;
                foreach (var elem in _contextData.Collection)
                {
                    if (elem.IsForThisContext(c))
                    {
                        context = elem.Context;
                        break;
                    }                                                                             
                }
                
                switch (context)
                {
                    case Context.None:
                        SetDefaultToLocalContext();
                        
                        _errors += $"simb {c} pos {_symbnumb} in line {_symbline} is not translate \n";
                        _symbnumb++;
                        break;
                    case Context.Space:
                        SetDefaultToLocalContext();
                        _symbnumb++;
                        break;
                    case Context.Line:
                        SetDefaultToLocalContext();
                        _symbline++;
                        _symbnumb = 0;
                        break;
                    case Context.Word:
                    {
                        _symbnumb++;
                        if (_localContext == context)
                            _memory += c;
                        else
                        {
                            SetDefaultToLocalContext();
                            _memory += c;
                            _localContext = Context.Word;
                          
                        }
                        break;
                    }
                    case Context.Const:
                        _symbnumb++;
                        if (_localContext == context)
                            _memory += c;
                        else
                        {
                            SetDefaultToLocalContext();
                            _memory += c;
                            _localContext = Context.Const;
                            
                        }
                        break;
                    case Context.Operation:
                        if (_localContext == Context.DoubleOperation && c == '=')
                        {
                            _memory += c;
                            _symbnumb++;
                            SetDefaultToLocalContext();
                        }
                        else
                        {
                            var index = _lib.GetOperationNumb(c.ToString());
                            SetDefaultToLocalContext();
                            if(index==-1)
                                _errors +=$" operation {c} pos {_symbnumb} in line {_symbline} is not translate \n";
                            else
                                _data.Add(index);
                        }
                        break;
                    case Context.DoubleOperation:
                    {
                        SetDefaultToLocalContext();
                        _symbnumb++;
                        _memory += c;
                        _localContext = Context.DoubleOperation;

                    }
                        break;
                    case Context.Special:
                    {
                        SetDefaultToLocalContext();
                        var index = _lib.GetSpecialNumb(c.ToString());
                        if(index==-1)
                            _errors +=$" special {c} pos {_symbnumb} in line {_symbline} is not translate \n";
                        else
                            _data.Add(index);
                        break;
                    }

                    default:
                        throw new ArgumentOutOfRangeException();
                }
              
            }
            SetDefaultToLocalContext();
        }

        private void SetDefaultToLocalContext()
        {
            switch (_localContext)
            {
                case Context.Word:
                {
                    _data.Add(_lib.GetWordNumb(_memory));
                    break;
                }
                case Context.Const:
                {
                    _data.Add(_lib.GetConstNumb(_memory));
                    break;
                }
                                    
                case Context.DoubleOperation:
                {
                    var i = _lib.GetDoubleNumb(_memory);
                    if(i==-1)
                        _errors +=$"double operation {_memory} pos {_symbnumb} in line {_symbline} is not translate \n";
                    else
                        _data.Add(i);
                    break;
                }                      
            }
            _localContext = Context.None;
            _memory = String.Empty;
        }

      
    }
}