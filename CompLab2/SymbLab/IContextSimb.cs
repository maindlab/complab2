namespace SymbLab
{
    public interface IContextSimb
    {
        Context Context { get; }
        bool IsForThisContext(char c);
    }
}