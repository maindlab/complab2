﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace PrepareLib
{
   public class Prepare
   {
      public static void StartPrepare(string outputFile, string inputFile)
      {
         using (var outStream =  new System.IO.StreamWriter(outputFile, true))
         using (var stream =new StreamReader(inputFile))
         {
           StartPrepare(outStream,stream);
         }
      }

      public static void StartPrepare(StreamWriter outStream, StreamReader stream)
      {
          var stat = true;
            var countSpace = 0;              
            var inComment = false;
            while (stat)
            {
               

               var data = stream.Read();
               if (data == -1) break;               
               var simb = (char) data;
               if (new Regex(@"\s").IsMatch(simb.ToString())&& !inComment && simb!='\n')
               {
                  countSpace++;
                  continue;
               }

               if (countSpace > 0)
               {
                  countSpace = 0;
                  WriteSimb(' ',outStream);
               }

               if (simb == '/')
               {
                  if (!inComment)
                  {
                     var cs = stream.Read();
                     switch (cs)
                     {
                        case -1:
                        {
                           stat = false;
                           WriteSimb(simb, outStream);
                           continue;
                        }
                        case '*':
                        {
                           inComment = true;
                           continue;
                        }
                        default:
                        {
                           WriteSimb(simb, outStream);
                           WriteSimb((char) cs, outStream);
                           continue;
                        }
                     }
                  }
               }
               if (simb == '*')
               {
                  if (inComment)
                  {
                     var cs = stream.Read();
                     switch (cs)
                     {
                        case -1:
                        {
                           stat = false;
                           WriteSimb(simb, outStream);
                           continue;
                        }
                        case '/':
                        {
                           inComment = false;
                           continue;
                        }
                        default:
                        {
                           WriteSimb(simb, outStream);
                           WriteSimb((char) cs, outStream);
                           continue;
                        }
                     }
                  }
               }
               if(!inComment || (inComment && simb=='\n'))
                WriteSimb(simb, outStream);
               
               
            }
      }
      private static void WriteSimb(char simb, StreamWriter streamWriter)
      {
         streamWriter.Write(simb);
      }
   }

}