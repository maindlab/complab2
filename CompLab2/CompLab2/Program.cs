﻿using System;
using System.IO;
using SymbLab;

namespace CompLab2
{
    class Program
    {
        static void Main(string[] args)
        {
            //using(File.WriteAllText("in.txt",String.Empty);
            //File.WriteAllText("out.txt",@"123 // /*  rere */ / *        1256 ** ?? // */ / ");
            File.WriteAllText("in.txt",String.Empty);
            PrepareLib.Prepare.StartPrepare( "in.txt","out.txt");
            Console.WriteLine(File.ReadAllText("in.txt"));
            SymbAutomat automat = new SymbAutomat(new ContextData(), new SymbLib());
            automat.StartConvert("in.txt");
        }
    }
}